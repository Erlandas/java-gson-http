package _001_http_request_method_one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Main_Refactored_1 {

    // Define connection
    private static HttpURLConnection connection;


    public static void main(String[] args) {

        String urlStringAlbums = "https://jsonplaceholder.typicode.com/albums";
        String responseContent = getData(urlStringAlbums);
        System.out.println(responseContent);

    }

    private static String getData(String urlString) {

        BufferedReader br;
        String eachLine;
        StringBuffer dataCollection = new StringBuffer();
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();

            // Setup request with 5 seconds time out for both connection and read
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            // Check status code
            if (connection.getResponseCode() > 299) {
                br =new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while((eachLine = br.readLine()) != null) {
                    dataCollection.append(eachLine + "\n");
                }
                br.close();
            } else {
                br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while((eachLine = br.readLine()) != null) {
                    dataCollection.append(eachLine + "\n");
                }
                br.close();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return dataCollection.toString();
    }
}
