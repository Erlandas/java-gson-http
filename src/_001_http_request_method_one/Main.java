package _001_http_request_method_one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {

    //Define connection
    private static HttpURLConnection connection;

    public static void main(String[] args) {

        //Needed for response input
        BufferedReader br;
        String line;
        StringBuffer responseContent = new StringBuffer();

        // Method 1: using: java.net.HttpURLConnection
        String urlStringAlbums = "https://jsonplaceholder.typicode.com/albums";
        try {
            URL url = new URL(urlStringAlbums);
            connection = (HttpURLConnection) url.openConnection();

            // Request setup
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            // first get response code to see if our connection is successful
            int statusCode = connection.getResponseCode();
            String statusMessage = connection.getResponseMessage();
            System.out.println("Status code : " + statusCode);
//            System.out.println("Status message : " + statusMessage);
            //String testString = "";
            //Get response from endpoint, the response we get is an input stream
            if (statusCode > 299) { // if wasn't successful
                br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while((line = br.readLine()) != null) {
                    responseContent.append(line);
                }
                br.close();
            } else {    // Successful connection
                br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while((line = br.readLine()) != null) {
                    responseContent.append(line + "\n");
                    //testString += line + "\n";
                }
            }
            System.out.println(responseContent.toString());
            //System.out.println(testString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

    }
}
