package _001_http_request_method_one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Main_Refactored_2 {

    private static HttpURLConnection con;

    public static void main(String[] args) {
        String urlStringAlbums = "https://jsonplaceholder.typicode.com/albums";
        if(establishConnection(urlStringAlbums)){
            System.out.println(getResponseData());
            closeConnection();
        }
    }

    private static String getResponseData() {
        BufferedReader br = null;
        try {
            if(con.getResponseCode() > 299) {
                //Connection failed with status code
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            } else {
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            }
            return retrieveData(br);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String retrieveData(BufferedReader br) {
        String line;
        StringBuilder dataResponse = new StringBuilder();
        while(true) {
            try (br) {
                while((line = br.readLine()) != null) {
                    dataResponse.append(line).append("\n");
                }
                return dataResponse.toString();
            } catch (IOException e) {
                System.out.println("Problem : " + e.getMessage());
            }
            return "";

        }

    }

    private static boolean establishConnection(String urlString) {
        try {
            URL url = new URL(urlString);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            return true;
        } catch (MalformedURLException e) {
            System.out.println("Problem : " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Problem : " + e.getMessage());
        }
        return false;
    }

    private static void closeConnection() {
        con.disconnect();
    }
}
