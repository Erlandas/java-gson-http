package _003_parse_data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Main_using_method_two {

    public static void main(String[] args) {
        String urlStringAlbums= "https://jsonplaceholder.typicode.com/albums";

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(urlStringAlbums)).build();
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                //.thenApply(Main_using_method_two::parse)
                .thenAccept(System.out::println)
                .join();
    }

    public static String parse(String responseBody) {
        JSONArray albums = new JSONArray(responseBody);
        for ( int i = 0 ; i < albums.length() ; i++ ) {
            JSONObject album = albums.getJSONObject(i);
            int id = album.getInt("id");
            int userId = album.getInt("userId");
            String title = album.getString("title");
            System.out.println(id + "\t" + title + "\t" + userId);
        }
        return null;
    }
}
