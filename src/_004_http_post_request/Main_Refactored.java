package _004_http_post_request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Main_Refactored {

    private static HttpURLConnection con;

    public static void main(String[] args) {

        sendPostRequest();
    }

    public static boolean sendPostRequest() {
        String urlString = "https://jsonplaceholder.typicode.com/posts";

        try {
            URL url = new URL(urlString);

            // Setup connection
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);



            // Adding headers
            con.setRequestProperty("Auth", "Token");
            con.setDoOutput(true);

            // Adding post data
            OutputStream os = con.getOutputStream();
            os.write("test1=test&test2=test".getBytes());
            os.flush();
            os.close();


            // check response code
            int responseStatus = con.getResponseCode();
            System.out.println("Response status : " + responseStatus);

            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder sb = new StringBuilder();
            while((line = br.readLine()) != null) {
                sb.append(line);
            }
            System.out.println("Response : " + sb);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
